package formatter 

import (
    jira "github.com/andygrunwald/go-jira"
)

type Ticket struct {
    Issue *jira.Issue
    Component string
    Status string
    TicketNum int
    String string
}
