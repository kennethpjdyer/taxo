package main

import (
    "fmt"
    "github.com/spf13/cobra"
    termbox "github.com/nsf/termbox-go"
    "github.com/spf13/viper"
    "runtime"
    "strings"
    "os"

    "gitlab.com/kennethpjdyer/taxo/v1/list"
    "gitlab.com/kennethpjdyer/taxo/v1/show"
    "gitlab.com/kennethpjdyer/taxo/v1/query"
    "gitlab.com/kennethpjdyer/taxo/v1/logger"
    "gitlab.com/kennethpjdyer/taxo/v1/utils"

)
const ver string = "0.1.0"
var (

	// CommitStamp - Provides shortened form of last commit on repository at build-time.
	CommitStamp string

	// BuildAt - Provides timestamp of last build.
	BuildAt string
)

/*
reportVersion function takes a boolean argument to indicate verobsity, and then 
prints to stdout information on the current version and built of Avocet Tools.
*/
func reportVersion(verbose bool) {
	var content []string
	if verbose {
		content = []string{
            "Taxo - A CLI-based Jira Client",
			"Kenneth P. J. Dyer <kenneth@avoceteditors.com>",
			"Avocet Editorial Consulting",
			fmt.Sprintf("Version: v%s", ver),
			fmt.Sprintf("Commit: %s", CommitStamp),
			fmt.Sprintf("Build Time: %s", BuildAt),
		}
	} else {
		content = []string{fmt.Sprintf("taxo - version %s\n", ver)}
	}
	fmt.Println(strings.Join(content, "\n  "))
}


/*
init function initilaizes the Viper configuration and sets any default values.
*/
func init(){
    viper.SetDefault("parallel", runtime.NumCPU())
    viper.SetDefault("verbosity", false)

    viper.SetDefault("user", "")
    viper.SetDefault("pwd", "")
    viper.SetDefault("url", "")

    viper.SetDefault("key", "")
    viper.SetDefault("consumer_key", "")
    viper.SetDefault("status_filter", "")
    viper.SetDefault("default_status", "status not in ('Closed', 'Resolved')")
    viper.SetDefault("sort_method", "ticket")
    viper.SetDefault("disable_colors", false)

    viper.SetDefault("name", "taxo")
    viper.SetDefault("projects", "DOCS,DOCSP") 

    viper.SetDefault("debug", false)
    viper.SetDefault("trace", false)

    // Retrieve Terminal Information
    if err := termbox.Init(); err != nil {
        logger.Fatal("Unable to initialize terminal box:\n", err)
    }
    width, height := termbox.Size()
    termbox.Close()
    viper.Set("termbox_width", width)
    viper.Set("termbox_height", height)

    // Queries
    viper.SetDefault("queries.triage", "")

    viper.AddConfigPath(".")
    viper.AddConfigPath("$HOME/.config/avocet")
    viper.SetConfigName("taxo.yml")
    viper.SetConfigType("yaml")
    err := viper.ReadInConfig()
    if err != nil {
        logger.Fatal(err)
    }
    viper.Set("key", utils.FixPath(viper.GetString("key")))
}

func main(){

    /******************** PERSISTENT OPTIONS ***********************/
    // Name
    cmd.PersistentFlags().StringP(
        "name", "n", viper.GetString("name"),
        "Sets the logical name of the connection")
    viper.BindPFlag("name", cmd.PersistentFlags().Lookup("name"))

    // Parallel
    cmd.PersistentFlags().IntP(
        "jobs", "j",
        viper.GetInt("parallel"),
        "Specifies the number of threads to utilize for specific tasks")
    viper.BindPFlag("parallel", cmd.PersistentFlags().Lookup("jobs"))
    // Password Flag
    cmd.PersistentFlags().StringP(
        "project", "p",
        viper.GetString("project"),
        "Sets the projects to search in list operations (accepts comma-separated variables)")
    viper.BindPFlag("projects", cmd.PersistentFlags().Lookup("projects"))

    cmd.PersistentFlags().StringP(
        "status", "s",
        viper.GetString("status_filter"),
        "Filters issues based on their current status")
    viper.BindPFlag("status_filter", cmd.PersistentFlags().Lookup("status_filter"))

    cmd.PersistentFlags().StringP(
        "sort", "S",
        viper.GetString("sort_method"),
        "Sets criterion for sorting tickets")
    viper.BindPFlag("sort_method", cmd.PersistentFlags().Lookup("sort_method"))

    // URL
    cmd.PersistentFlags().StringP(
        "url", "U",
        viper.GetString("url"),
        "Sets the URL to your Jira")
    viper.BindPFlag("url", cmd.PersistentFlags().Lookup("url"))

    // User Flag
    cmd.PersistentFlags().StringP(
        "user", "u",
        viper.GetString("user"),
        "Sets the user to use when connecting to Jira")
    viper.BindPFlag("user", cmd.PersistentFlags().Lookup("user"))

    // Verbosity Flag
    cmd.PersistentFlags().BoolP(
        "verbose", "v",
        viper.GetBool("verbosity"),
        "Enables verbose output")
    viper.BindPFlag("verbosity", cmd.PersistentFlags().Lookup("verbose"))

    // Debug
    cmd.PersistentFlags().BoolP(
        "debug", "D",
        viper.GetBool("debug"),
        "Enables debugging output for logs")
    viper.BindPFlag("debug", cmd.PersistentFlags().Lookup("debug"))
 
    // Trace
    cmd.PersistentFlags().BoolP(
        "trace", "T",
        viper.GetBool("trace"),
        "Enables trace output for logs")
    viper.BindPFlag("trace", cmd.PersistentFlags().Lookup("trace"))


    /********************* SUBCOMMANDS ****************************/

    // List Command
    cmd.AddCommand(cmdList)

    // Show Command
    cmd.AddCommand(cmdShow)

    // Version
    cmd.AddCommand(cmdVer)

    cmd.AddCommand(queryCmd)

    /********************* EXECUTE COMMAND ************************/
    cmd.Execute()
}

var cmd = &cobra.Command{
    Use: "taxo",
    Short: "A CLI Client for interacting with Jira repositories.",
    Run: func (cmd *cobra.Command, args []string){
        reportVersion(viper.GetBool("verbosity"))
        os.Exit(0)
    },
    PersistentPreRun: func (cmd *cobra.Command, args []string){
        logger.Trace("Starting Taxo")
    },
}

var queryCmd = &cobra.Command{
    Use: "query",
    Short: "Executes arbitrary query from configuration file",
    Run: func (cmd *cobra.Command, args []string){
        var queries map[string]map[string]string
        viper.UnmarshalKey("queries", &queries)
        for _, key := range args {
            query.RunArbitraryQuery(queries[key]["query"], queries[key]["fields"])
        }
        os.Exit(0)
    },
}

var cmdList = &cobra.Command{
    Use: "list",
    Short: "Lists tickets assigned to you or others.",
    Run: func (cmd *cobra.Command, args []string){
        list.Run(args)
    },
}

var cmdShow = &cobra.Command{
    Use: "show",
    Short: "Prints ticket information.",
    Run: func (cmd *cobra.Command, args []string){
        show.Run(args)
    },
}

var cmdVer = &cobra.Command{
    Use: "version",
    Short: "Reports version information to stdout.",
    Run: func (cmd *cobra.Command, args []string){
        reportVersion(viper.GetBool("verbosity"))
        os.Exit(0)
    },
}
