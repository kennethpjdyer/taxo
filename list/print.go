package list

import (
    "fmt"
    jira "github.com/andygrunwald/go-jira"
    "strings"
    "gitlab.com/kennethpjdyer/taxo/v1/formatter"
    "gitlab.com/kennethpjdyer/taxo/v1/logger"
    "gitlab.com/kennethpjdyer/taxo/v1/utils"
    "github.com/spf13/viper"
)


func formatRow(issue jira.Issue, verbosity bool, sumwidth int) string {
    row := []string{}

    row = append(row, issue.Key)

    // Add Status Line
    row = append(row, formatter.FormatStatus(issue.Fields.Status.Name, verbosity))

    assignee := issue.Fields.Assignee
    if assignee != nil {
        row = append(row, assignee.DisplayName) 
    }
    text := issue.Fields.Summary
    if !verbosity && len(text) > sumwidth {
        text = text[0:sumwidth] + "..."
    }    
    row = append(row, text)

    return strings.Join(row, "\t")

}


func printIssues(issues []jira.Issue){
    logger.Trace("Print Issues List")
    defer logger.Report(logger.Time("Printing of Issues List Complete"))

    // Initialize Workers
    issueChan := make(chan jira.Issue, len(issues))
    ticketChan := make(chan formatter.Ticket, len(issues))

    for w := 0 ; w < viper.GetInt("parallel"); w++ {
        go formatter.RowWorker(w, issueChan, ticketChan, formatRow)
    }

    // Format Rows
    for _, issue := range issues {
        issueChan <- issue
    }
    close(issueChan)

    tickets := []formatter.Ticket{}
    for i := 0; i < len(issues); i++ {
        tickets = append(tickets, <-ticketChan)
    }
    close(ticketChan)

    // Sort Issues
    //tickets = utils.SortTickets(tickets)

    // Extract Rows
    rows := []string{}
    for _, ticket := range tickets {
        rows = append(rows, ticket.String)
    }
    rows = append(rows, "\n")


    // Tabprint
    utils.TabPrint(rows)
}
