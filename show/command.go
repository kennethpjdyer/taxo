package show

import (
    "gitlab.com/kennethpjdyer/taxo/v1/connect"
    "gitlab.com/kennethpjdyer/taxo/v1/logger"
)

func Run(ticket []string){
    logger.Trace("Called Show Operation")
    defer logger.Report(logger.Time("Show Operation Complete"))
    if len(ticket) == 1 {
        client := connect.GetClient()
        issue, _, e := client.Issue.Get(ticket[0], nil)
        if e != nil {
            logger.Fatal("Unable to retrieve Jira Ticket:\n", e)
        }
        printIssue(issue)
    }
}
