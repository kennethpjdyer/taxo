package query

import (
    "github.com/spf13/viper"
)

func sortQuery() string{
    switch viper.GetString("sort_method"){
    case "assignee":
        return " ORDERY BY assignee ASC"
    default:
        return " ORDER BY key ASC"
    }
}
