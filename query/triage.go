package query

import (
    "strings"
    "github.com/spf13/viper"
    jira "github.com/andygrunwald/go-jira"
    "gitlab.com/kennethpjdyer/taxo/v1/connect"
)

func ListTriage(client *jira.Client) []jira.Issue {

    query := viper.GetString("queries.triage") 
    return runQuery(client, query, []string{"assignee", "key", "summary", "created"})

}

func RunArbitraryQuery(query string, strFields string){
    client := connect.GetClient()
    fields := strings.Split(strFields, ",")
    issues := runQuery(client, query, fields)
    printIssues(issues)
}


