package utils

import (
    "fmt"
    jira "github.com/andygrunwald/go-jira"
    "gitlab.com/kennethpjdyer/taxo/v1/logger"
    "os"
    "strings"
)


func FixPath(p string) string{
    if string(p[0]) == "~" {
        hm, err := os.UserHomeDir()
        if err != nil {
            logger.Fatal(err)
            return p
        }
        return fmt.Sprintf("%s/%s", hm, p[1:])
    }
    return p
}


func IssueSort(input []jira.Issue) []jira.Issue {
    var temp jira.Issue
    for i := 0; i < len(input); i++ {
        for j := i + 1; j < len(input); j++ {
            if strings.Compare(input[i].Key, input[j].Key) > 0 {
                temp = input[i]
                input[i] = input[j]
                input[j] = temp
            }
        }
    }
    return input
}

