package query 

import (
    "github.com/spf13/viper"
    jira "github.com/andygrunwald/go-jira"
    "strings"
    "gitlab.com/kennethpjdyer/taxo/v1/utils"
    "gitlab.com/kennethpjdyer/taxo/v1/formatter"
)

func formatRow(issue jira.Issue, verbosity bool, sumwidth int) string {
    row := []string{}
    // Ticket Identifier
    row = append(row, issue.Key)

    // Status Line
    row = append(row, formatter.FormatStatus(issue.Fields.Status.Name, verbosity))

    // Assignee
    assignee := issue.Fields.Assignee
    if assignee != nil {
        row = append(row, assignee.DisplayName) 
    } else {
        row = append(row, "Unassigned")
    }


    // Summary
    text := issue.Fields.Summary
    if !verbosity && (len(text) > sumwidth) {
        text = text[0:sumwidth] + "..."
    }
    row = append(row, text)

    return strings.Join(row, "\t")
}

func printIssues(issues []jira.Issue){

    issueChan := make(chan jira.Issue, len(issues))
    ticketChan := make(chan formatter.Ticket, len(issues))

    for w := 0; w < viper.GetInt("parallel"); w++ {
        go formatter.RowWorker(w, issueChan, ticketChan, formatRow)
    }

    // Format Rows
    for _, issue := range issues {
        issueChan <- issue
    }
    close(issueChan)

    tickets := []formatter.Ticket{}
    for i := 0; i < len(issues); i++ {
        tickets = append(tickets, <-ticketChan)
    }
    close(ticketChan)

    // Extract Rows
    rows := []string{}
    for _, ticket := range tickets {
        rows = append(rows, ticket.String)
    }
    rows = append(rows, "\n")

    // Tabprint
    utils.TabPrint(rows)
}
