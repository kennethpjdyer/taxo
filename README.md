# Taxo

A CLI client for Jira written in Go.

## Install

1. Initialize directories: 
   ```sh
   $ cd
   $ mkdir go
   $ mkdir go/{src,bin,pkg}
   $ mkdir -p go/src/gitlab.com/kennethpjdyer
   ```

1. Clone the Taxo repository:

   ```sh
   $ cd go/src/gitlab.com/kennethpjdyer
   $ git clone git:gitlab.com/kennethpjdyer/taxo
   ```

1. Install Taxo:

   ```sh
   $ cd taxo
   $ make
   ```
   
## Usage

## Roadmap

The sections below outline plans for Taxo.

### List

The `list` command (alias `ls`) should list tickets retrieved from Jira.  It should default to listing tasks assigned to the current user, but also provide the option of listing all tasks with a given component.

Example:

```sh
$ taxo list
```

Example Output: 

```
DOCS-10   (STATUS) author.name description from title
DOCS-2000 (STATUS) author.name description from title
DOCS-300  (STATUS) author.name description from title
```

#### Special Arguments

The `list` command should accept arguments to serve as an assignment filter.

The first check should pair the argument with a series of special categories like `all`
or `unassigned`.

The second check should try to match the argument against a map key for email addresses,
providing a configurable shorthand to find and match specific users.

The last check should use the argument as an email address in matching assignees.


#### Status Filter

The `list` command should also have options for listing tickets in particular
states, such as only showing those in-progress or open.

```sh
$ taxo list --status progress
```

Mapping of simple strings to usernames and statuses should be user-defined through
the configuration file.


## Show Command

The `show` command should render the given ticket with relevant information to stdout, removing the need to navigate to the Jira website.

```sh
$ taxo show DOCS-1000
```

## Count Command

The `count` command provides a simple interface for getting a count of tickets in a particular state:

```sh
$ taxo count
```

Example Output:

```
Open:            24
In-Progress:     30
Internal Review:  1
External Review:  3
```

## Todo List

* Read configuration file.

* Retrieve Jira token from file, error if finds malconfigured file privileges.

* Connect to Jira. 

* Retrieve open tickets for arbitrary user, default user to current.

* Tabprint issues and descriptions

* Tabprint issues in swim lanes

