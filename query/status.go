package query

import (
    "fmt"
    "github.com/spf13/viper"
)

func getStatus() string {
    var ret string
    status := viper.GetString("status_filter")
    if status != "" {
        ret = fmt.Sprintf("status %s", status)
    } else {
        ret = viper.GetString("default_status")
    }

    return ret
}
