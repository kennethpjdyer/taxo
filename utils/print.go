package utils 
import (
    "fmt"
    "os"
    "strings"
    "text/tabwriter"
)

func TabPrint(rows []string){
    // Tabprint
    w := tabwriter.NewWriter(os.Stdout, 3, 0, 3, ' ', tabwriter.TabIndent)
    fmt.Fprintf(w, strings.Join(rows, "\n"))
    w.Flush()

}
