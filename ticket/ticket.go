package ticket
import (
    jira "github.com/andygrunwald/go-jira"
)

type Ticket struct {
    Issue jira.Issue
    String string
}

