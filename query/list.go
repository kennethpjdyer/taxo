package query

import (
    "fmt"
    "strings"
    "github.com/spf13/viper"
    "gitlab.com/kennethpjdyer/taxo/v1/logger"
    jira "github.com/andygrunwald/go-jira"
)


func ListIssues(client *jira.Client, args []string) []jira.Issue {
    logger.Trace("Retrieve issues list from Jira")
    defer logger.Report(logger.Time("Retrival of issues list complete"))
    jql := []string{}

    // Fetch Assignee
    assignees := getAssignee(args)
    if assignees != "" {
        jql = append(jql, assignees)
    } else {
        // Projects
        projects := []string{}
        for _, p := range strings.Split(viper.GetString("projects"), ",") {
            projects = append(projects, fmt.Sprintf("'%s'", p))
        }
        if len(projects) > 0 {
            jql = append(jql, fmt.Sprintf("project in (%s)", strings.Join(projects, ",")))
        }
    }

    // Fetch Status
    status := getStatus()
    if status != "" {
        jql = append(jql, status)
    }
    
    query := strings.Join(jql, " and ")
    query += sortQuery()

    return runQuery(client, query, []string{"key", "assignee", "summary", "status"}) 

}
