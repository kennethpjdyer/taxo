package show

import (
    //"fmt"
    "strings"
    jira "github.com/andygrunwald/go-jira"
    //"github.com/spf13/viper"
    "gitlab.com/kennethpjdyer/taxo/v1/logger"
    "gitlab.com/kennethpjdyer/taxo/v1/utils"
    "github.com/spf13/viper"
    fmt "github.com/jhunt/go-ansi"
)

func newLine(content []string) []string{
    return append(content, "\n")
}


func printIssue(issue *jira.Issue){
    logger.Trace("Printing Issue")

    // Print Title Line
    content := []string{}
    title := fmt.Sprintf("%s: %s", issue.Key, issue.Fields.Summary)
    content = append(content, title)
    content = newLine(content)
    fmt.Printf("@G{%s}", strings.Join(content, " "))

    // Print Metadata
    content = []string{}
    setLink(issue, &content)
    setAssignee(issue, &content)
    setReporter(issue, &content)
    setStatus(issue, &content)
    setType(issue, &content)
    setEpic(issue, &content)
    setComponentData(issue, &content)

    content = newLine(content)
    utils.TabPrint(content)
    content = []string{}

    setDescription(issue, &content)
    content = newLine(content)
    fmt.Printf(strings.Join(content, "\n"))

    if viper.GetBool("verbosity"){
        setComments(issue, &content)
        fmt.Printf("%s\n", strings.Join(content, "\n"))
    }
}
