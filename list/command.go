package list

import (
    "fmt"
    "os"
    "gitlab.com/kennethpjdyer/taxo/v1/connect"
    "gitlab.com/kennethpjdyer/taxo/v1/logger"
    "gitlab.com/kennethpjdyer/taxo/v1/query"
)

func Run(args []string){
    logger.Trace("Called the List Operation")
    defer logger.Report(logger.Time("List Operation Completed"))

    client := connect.GetClient()
    issues := query.ListIssues(client, args)

    if len(issues) == 0 {
        fmt.Println("No issues found")
        os.Exit(0)
    }
    printIssues(issues)
}
