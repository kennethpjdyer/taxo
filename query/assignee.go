package query

import (
    "fmt"
    "github.com/spf13/viper"
    "strings"
)

func getAssignee(assignees []string) string {
    jql := []string{}
    if len(assignees) == 0 {
        jql = append(jql, fmt.Sprintf("assignee='%s'", viper.GetString("user")))
    } else {
        var pattern string
        listAssignees := []string{}
        for _, a := range assignees {
            pattern = fmt.Sprintf("users.%s", a)
            if viper.GetString(pattern) != "" {
                listAssignees = append(listAssignees, fmt.Sprintf("'%s'", viper.GetString(pattern)))
            } else {
                listAssignees = append(listAssignees, fmt.Sprintf("'%s'", a))
            }
        }
        jql = append(jql, fmt.Sprintf("assignee in (%s)", strings.Join(listAssignees, ",")))
    }

    return strings.Join(jql, " or ")
}
