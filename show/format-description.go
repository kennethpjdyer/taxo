package show
import (
    jira "github.com/andygrunwald/go-jira"
    "strings"
)


func setDescription(issue *jira.Issue, content *[]string){
    descr := issue.Fields.Description
    descrLines := strings.Split(descr, "\n")

    *content = append(*content, strings.Join(descrLines, "\n"))

}
