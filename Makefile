
COMMIT_VALUE=`git rev-parse --short HEAD`
COMMIT=-ldflags "-X main.CommitStamp=$(COMMIT_VALUE)"

BUILD_AT_VALUE=`date +%FT%T%z`
BUILD_AT=-ldflags "-X main.BuildAt=$(BUILD_AT_VALUE)"

BFLAGS=$(COMMIT) $(BUILD_AT)  

install:
	go install $(BFLAGS) -mod vendor ./cmd/taxo
build: 
	go build $(BFLAGS) -o dist/taxo -mod vendor ./cmd/taxo





