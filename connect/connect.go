package connect

import (
    "crypto/rsa"
    "crypto/x509"
    "encoding/pem"
    "fmt"
    "github.com/spf13/viper"
    "github.com/dghubble/oauth1"
    "gitlab.com/kennethpjdyer/taxo/v1/logger"
    "golang.org/x/net/context"
    "io/ioutil"
    jira "github.com/andygrunwald/go-jira"
    "net/http"
    "os"
    "strings"
)

var jiraURL = viper.GetString("url")

func getKey() *rsa.PrivateKey{
    raw, err := ioutil.ReadFile(viper.GetString("key"))
    if err != nil {
        logger.Fatal(err)
        os.Exit(1)
    }
    keyPem, _ := pem.Decode([]byte(raw))
    if keyPem == nil {
        logger.Fatal("Unable to decode PEM key")
    }
    viper.Set("keyPem", raw)

    if !(keyPem.Type == "PRIVATE KEY" || strings.HasSuffix(keyPem.Type, " PRIVATE KEY")) {
        logger.Fatal("Unexpected Key Type:", keyPem.Type)
    }
    privateKey, pKeyErr := x509.ParsePKCS1PrivateKey(keyPem.Bytes)
    if pKeyErr != nil {
        logger.Fatal("Unable to parse private key:\n", pKeyErr)
    }

    return privateKey
}

func getJiraHTTPClient(ctx context.Context, config *oauth1.Config) *http.Client{
    tok := oauth1.NewToken(viper.GetString("access_token"), viper.GetString("access_secret") )

    return config.Client(ctx, tok)
}

func GetClient() *jira.Client{
    logger.Trace("Fetch Jira Client")
    defer logger.Report(logger.Time("Retrieval of Jira Client"))

    ctx := context.Background()
    key := getKey()

    signer := &oauth1.RSASigner{ PrivateKey: key }

    config := oauth1.Config{
        ConsumerKey: viper.GetString("consumer_key"),
        CallbackURL: jiraURL,
        Endpoint: oauth1.Endpoint{
            RequestTokenURL: fmt.Sprintf("%splugins/servlet/oauth/request-token", jiraURL),
            AuthorizeURL: fmt.Sprintf("%splugins/servlet/oauth/authorize", jiraURL),
            AccessTokenURL: fmt.Sprintf("%splugins/servlet/oauth/access-token", jiraURL),
        },
        Signer: signer,
    }

    client, err := jira.NewClient(getJiraHTTPClient(ctx, &config), viper.GetString("url"))
    if err != nil {
        logger.Fatal("Unable to create Jira Client:\n", err)
    }

    return client
}
