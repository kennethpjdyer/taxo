package formatter
import (
    "fmt"
    "github.com/spf13/viper"
    jira "github.com/andygrunwald/go-jira"
    "gitlab.com/kennethpjdyer/avocet/color"
    "strconv"
    "strings"
)
func FormatStatusText(status string) string {
    switch status {
    case "Needs Triage":
        return "Triage"
    case "In Progress":
        return "Progress"
    case "Internal Review":
        return "Int Rev"
    case "External Review":
        return "Ext Rev"
    case "Resolved":
        return "Rs"
    case "Done":
        return "Dn"
    default:
        return status
    }
}
func FormatStatus(status string, verbosity bool) string {
    var onset string
    switch status {
    case "Needs Triage":
        onset = color.BoldCyan
    case "Open":
        onset = color.Yellow
    case "In Progress":
        onset = color.BoldYellow
    case "Internal Review":
        onset = color.Purple
    case "External Review":
        onset = color.BoldPurple
    case "Resolved":
        onset = color.Green
    case "Done":
        onset = color.BoldGreen
    case "Blocked":
        onset = color.BoldRed
    }
    if verbosity {
        status = fmt.Sprintf("%s%s%s", onset, FormatStatusText(status), color.Reset)
    } else {
        status = fmt.Sprintf("%s%s%s", onset, FormatStatusText(status), color.Reset)
    }
    return status
}

func setTicketNumber(tick *Ticket){
    splits := strings.Split(tick.Issue.Key, "-")
    tick.Component = splits[0]
    tick.TicketNum, _ = strconv.Atoi(splits[1]) 
}

func RowWorker(id int, issueChan <-chan jira.Issue, ticketChan chan<- Ticket, 
        rowFormatter func(jira.Issue, bool, int) string){
    var tick Ticket
    verbosity := viper.GetBool("verbosity")
    sumwidth := 0
    if !verbosity {
        sumwidth = viper.GetInt("termbox_width") - 60
    }
    for issue := range issueChan {
        tick = Ticket{
            Issue: &issue,
            String: rowFormatter(issue, verbosity, sumwidth),
        }
        setTicketNumber(&tick)
        ticketChan <- tick
    }
}


