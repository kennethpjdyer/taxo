package show
import (
    "fmt"
    jira "github.com/andygrunwald/go-jira"
    "github.com/spf13/viper"

    "strings"
)

func setLink(issue *jira.Issue, content *[]string){
    *content = append(
        *content, 
        fmt.Sprintf(
            "- Link:\t%s/browse/%s",
            viper.GetString("url"),
            issue.Key))
}

func setAssignee(issue *jira.Issue, content *[]string){
    assignee := issue.Fields.Assignee
    person := "Unassigned"
    if assignee != nil {
        person = assignee.DisplayName
    }
    *content = append(
        *content,
        fmt.Sprintf( "- Assignee:\t%s", person))
}

func setReporter(issue *jira.Issue, content *[]string){
    reporter := issue.Fields.Reporter
    if reporter != nil {
        *content = append(
            *content,
            fmt.Sprintf("- Reporter:\t%s", reporter.DisplayName))
    }
}

func setStatus(issue *jira.Issue, content *[]string){
    *content = append(
        *content,
        fmt.Sprintf("- Status:\t%s", issue.Fields.Status.Name))
    if issue.Fields.Resolution != nil {
        *content = append(
            *content,
            fmt.Sprintf("- Resolution:\t%s", issue.Fields.Resolution.Name))
    }
}

func setType(issue *jira.Issue, content *[]string){
    *content = append(*content, fmt.Sprintf("- Type:\t%s", issue.Fields.Type.Name))
}

func setPriority(issue *jira.Issue, content *[]string){
    *content = append(*content, fmt.Sprintf("- Priority:\t%s", issue.Fields.Priority.Name))
}

func setComponentData(issue *jira.Issue, content *[]string){
    // Components
    comps := []string{}
    for _, comp := range issue.Fields.Components {
        comps = append(comps, comp.Name)
    }
    if len(comps) > 0 {
        *content = append(*content, fmt.Sprintf("- Components:\t%s", strings.Join(comps, ", ")))
    }

    // Labels
    if len(issue.Fields.Labels) > 0 {
        *content = append(*content, fmt.Sprintf("- Labels:\t%s", strings.Join(issue.Fields.Labels, ", ")))
    }

    // Fix Versions
    vers := []string{}
    for _, fix := range issue.Fields.FixVersions {
        vers = append(vers, fix.Name)
    }
    if len(vers) > 0 {
        *content = append(*content, fmt.Sprintf("- Fixes:\t%s", strings.Join(vers, ", ")))
        vers = []string{}
    }
    for _, aff := range issue.Fields.AffectsVersions {
        vers = append(vers, aff.Name)
    }
    if len(vers) > 0 {
        *content = append(*content, fmt.Sprintf("- Affects:\t%s", strings.Join(vers, ", ")))
    }

}

func setEpic(issue *jira.Issue, content *[]string){
    epic := issue.Fields.Unknowns["customfield_10858"]
    if epic != nil {
        *content = append(*content, fmt.Sprintf("- Epic:\t%s", epic))
    }
}
