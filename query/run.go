package query


import (
    "gitlab.com/kennethpjdyer/taxo/v1/logger"
    "strings"
    jira "github.com/andygrunwald/go-jira"
)

func runQuery(client *jira.Client, query string, fields []string) []jira.Issue {
    issues, _, err := client.Issue.Search(query, &jira.SearchOptions{
        MaxResults: 100,
        Fields: fields,
    })
    if err != nil {
        logger.Fatal("Jira Query Failed:\n  Q:", query, "\n  Fields:", strings.Join(fields, ", "), "\n", err)
    }
    return issues
}
