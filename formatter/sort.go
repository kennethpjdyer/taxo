package formatter

import (
    "strings"
    "github.com/spf13/viper"
)

func sortTicketsByTicketKey(tickets []Ticket) []Ticket{
    var tmp Ticket
    for i := 0; i < len(tickets); i++ {
        for j := 0; j < len(tickets); j++ {
            if strings.Compare(tickets[i].Issue.Key, tickets[j].Issue.Key) > 0{
                tmp = tickets[i]
                tickets[i] = tickets[j]
                tickets[j] = tmp
            }
        }
    }

    return tickets
}

func SortTickets(tickets []Ticket) []Ticket{
    switch viper.GetString("sort_method"){
    case "ticket":
        return sortTicketsByTicketKey(tickets)
    default:
        return tickets
    }
}
