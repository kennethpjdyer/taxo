package show

import (
    fmt "github.com/jhunt/go-ansi"
    jira "github.com/andygrunwald/go-jira"
    "strings"
)

func setComments(issue *jira.Issue, content *[]string){
    if issue.Fields.Comments != nil {
        *content = []string{fmt.Sprintf("\n\n@b{Comments:}")}
        var lines []string
        for _, com := range issue.Fields.Comments.Comments {
            *content = append(
                *content,
                fmt.Sprintf(
                    "@c{%s <%s> (%s)}", 
                    com.Author.DisplayName, 
                    com.Author.EmailAddress, 
                    com.Author.TimeZone))
            lines = []string{}
            for _, base := range strings.Split(com.Body, "\n"){
                lines = append(lines, fmt.Sprintf("  %s", base))
            }
            *content = append(
                *content,
                strings.Join(lines, "\n"))
            *content = append(*content, "\n")
        }
    }
}
